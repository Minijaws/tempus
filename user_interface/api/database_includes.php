<?php

/*
 * FILE         : database_includes.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Database information to be used in the other PHP files. Config!
 */

$dbname = "risk_scenarios";
$dbhost = "localhost";
$dbuser = "tempus";
$dbpw = "tempus123";