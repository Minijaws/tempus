<?php
/**
 * Created by PhpStorm.
 * User: awsom
 * Date: 2017-03-15
 * Time: 2:53 PM
 */

function PostRiskScenario($scenario)
{
    // Connect to database
    $servername = "localhost";
    $username = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=risk_scenarios", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully";
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }
    // Call queries to add $scenario to the database
    // Add name and iterations in Scenarios table
    // Add event frequency min/max/ml/confidence in Event Frequencies table
    // Add loss magnitude min/max/ml/confidence in Loss Magnitudes table
    // Add threat capability min/max/ml/confidence in Threat Capabilities table
    // Return success or fail
}

function DeleteRiskScenario($scenarioId)
{
    // Connect to database
    // Call query to delete $scenarioId from the database
    // Return success or fail
}

function GetRiskScenario($scenarioId)
{
    // Connect to database
    // Call queries to get scenario with the given ID
		// Get name
		// Get event frequency
		// Get loss magnitude
		// Get control strength
		// Get threat capability
	// Return a JSON object
}

function PutRiskScenario($scenarioId, $scenario)
{
    // Connect to database
    // Call queries to update $scenarioId in the database
    // Modify name and iterations in Scenarios table
    // Modify event frequency min/max/ml/confidence in Event Frequencies table
    // Modify loss magnitude min/max/ml/confidence in Loss Magnitudes table
    // Modify threat capability min/max/ml/confidence in Threat Capabilities table
    // Return success or fail
}

function GetAllRiskScenarios()
{
    // Connect to database
    $servername = "localhost";
    $username = "root";
    $password = "";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=risk_scenarios", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully";
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }
	// Call query to get all scenarios
	// For each scenario:
        // Get name
		// Get id
		// Get event frequency
		// Get loss magnitude
		// Get control strength
		// Get threat capability
	// Return all scenarios as array of JSON objects
}

function PostCSV($csv)
{
    // Get file
	// Read line by line
	// On each line, split by commas
	// Assign each chunk of the line to a variable
	// Put each variable into temporary classes
		// Scenario
			// LEF
			// TC
			// LM
			// CS
	// Connect to database
	// For each risk scenario:
        // PostRiskScenario()
	// Return success or fail
}

function GetGraphInformation($scenarioId, $graphType)
{
    // Connect to database
	// Call query to get graph information
	// Assemble TSV using graph information
	// Return TSV
}