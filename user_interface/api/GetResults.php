<?php
/*
 * FILE         : GetResults.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to get simulation results from the database
 */

include 'Scenario.php';
include 'database_includes.php';

header('Content-type: application/json');

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

if(isset($_GET['id'])) {
    $id = $_GET['id'];

    $statement=$pdo->prepare("SELECT Loss, LossEvents FROM results WHERE scenarioID = :id ORDER BY Loss ASC");
    $statement->execute(array($id));
    $results=$statement->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($results);
}
else {
    $statement=$pdo->prepare("SELECT ScenarioID, Loss, LossEvents FROM results ORDER BY Loss ASC");
    $statement->execute();
    $results=$statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
    echo json_encode($results);
}


