<?php
/*
 * FILE         : Scenario.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker, Joshua Gowing
 * DATE         : March 2017
 * DESCTIPTION  : Used to define what a scenario is
 */

class ThreatEventFrequency
{
    public $max; //double
    public $ml; //double
    public $min; //double
    public $confidence; //int
    public $shape; //String
}

class LossMagnitude
{
    public $max; //double
    public $ml; //double
    public $min; //double
    public $confidence; //int
    public $shape; //String
}

class ThreatCapability
{
    public $max; //double
    public $ml; //double
    public $min; //double
    public $confidence; //int
    public $shape; //String
}

class ControlStrength
{
    public $max; //double
    public $ml; //double
    public $min; //double
    public $confidence; //int
    public $shape; //String
}

class Scenario
{
    public $name; //String
    public $iterations; //int
    public $threat_event_frequency; //ThreatEventFrequency
    public $loss_magnitude; //LossMagnitude
    public $threat_capability; //ThreatCapability
    public $control_strength; //ControlStrength
}