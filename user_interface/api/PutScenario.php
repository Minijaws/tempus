<?php
/*
 * FILE         : PutScenario.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to modify a scenario in the database
 */

include 'Scenario.php';
include 'database_includes.php';

$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON);

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$statement = $pdo->prepare("UPDATE scenarios
                            SET isCalculated = :isCalculated,
                                name = :name, 
                                iterations = :iterations, 
                                TEFMin = :TEFMin, 
                                TEFML = :TEFML, 
                                TEFMax = :TEFMax, 
                                TEFConf = :TEFConf, 
                                TEFShape = :TEFShape, 
                                CSMin = :CSMin, 
                                CSML = :CSML, 
                                CSMax = :CSMax, 
                                CSConf = :CSConf, 
                                CSShape = :CSShape, 
                                TCMin = :TCMin, 
                                TCML = :TCML, 
                                TCMax = :TCMax, 
                                TCConf = :TCConf, 
                                TCShape = :TCShape, 
                                LMMin = :LMMin, 
                                LMML = :LMML, 
                                LMMax = :LMMax, 
                                LMConf = :LMConf, 
                                LMShape = :LMShape
                            WHERE id = :scenarioID;");
$statement->execute(array(
    "isCalculated" => false,
    "name" => $input->name,
    "iterations" => $input->iterations,
    "scenarioID" => $input->id,
    "TEFMin" => $input->TEF->min,
    "TEFML" => $input->TEF->ml,
    "TEFMax" => $input->TEF->max,
    "TEFConf" => $input->TEF->confidence,
    "TEFShape" => $input->TEF->shape,
    "CSMin" => $input->CS->min,
    "CSML" => $input->CS->ml,
    "CSMax" => $input->CS->max,
    "CSConf" => $input->CS->confidence,
    "CSShape" => $input->CS->shape,
    "TCMin" => $input->TC->min,
    "TCML" => $input->TC->ml,
    "TCMax" => $input->TC->max,
    "TCConf" => $input->TC->confidence,
    "TCShape" => $input->TC->shape,
    "LMMin" => $input->LM->min,
    "LMML" => $input->LM->ml,
    "LMMax" => $input->LM->max,
    "LMConf" => $input->LM->confidence,
    "LMShape" => $input->LM->shape
));