<?php
/**
 * FILE         : RunSimulation.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Joshua Gowing
 * DATE         : February 2017
 * DESCRIPTION  : Gets called when the web interface wants to update the current simulations. Gathers the
 *                data from the database for those simulations and sends it to all the workers.
 */

include 'database_includes.php';

function var_dump_pre($mixed = null) {
    echo '<pre>';
    var_dump($mixed);
    echo '</pre>';
    return null;
}
$context = new ZMQContext();
$sender = new ZMQSocket($context,ZMQ::SOCKET_PUSH);
$sender->bind("tcp://*:5557");
$receiver = new ZMQSocket($context, ZMQ::SOCKET_PULL);
$receiver->bind("tcp://*:5558");
$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$statement=$pdo->prepare("SELECT * FROM scenarios WHERE isCalculated = 0");
$statement->execute();
$results=$statement->fetchAll(PDO::FETCH_ASSOC);

$sum=0;
foreach ($results as $line){
    $iterations = $line['iterations'];
    $line['iterations'] = 2000;
    for ($i = 0; $i < $iterations; $i += $line['iterations']){
        var_dump_pre(json_encode($line));
        $sender->send(json_encode($line));
        $sum++;
    }
}
$insert=$pdo->prepare("INSERT INTO results(scenarioID,Loss,LossEvents) VALUES(:id, :loss, :lossEvents)");
$update = $pdo->prepare("UPDATE scenarios SET isCalculated = 1 WHERE id = :id");
$pdo->beginTransaction();

for($i = 0; $i < $sum; $i++){
    $message = $receiver->recv();
    $data = json_decode($message);
    var_dump_pre($message);
    $id = $data->id;
    foreach ($data->Losses as $loss){
        $insert->execute(array($id,$loss->Loss,$loss->LossEvents));
    }
    $update->execute(array($id));
    //var_dump_pre();
}
$pdo->commit();



