<?php
/*
 * FILE         : PostScenario.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to insert a scenario into the database
 */

include 'Scenario.php';
include 'database_includes.php';

$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON);

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$statement = $pdo->prepare("INSERT INTO scenarios(isCalculated, name, iterations, 
                                                    TEFMin, TEFML, TEFMax, 
                                                    TEFConf, TEFShape, CSMin, 
                                                    CSML, CSMax, CSConf, 
                                                    CSShape, TCMin, TCML, 
                                                    TCMax, TCConf, TCShape, 
                                                    LMMin, LMML, LMMax, 
                                                    LMConf, LMShape)
    VALUES(:isCalculated, :name, :iterations, :TEFMin, :TEFML, :TEFMax, :TEFConf, :TEFShape, :CSMin, :CSML, :CSMax, :CSConf, :CSShape, :TCMin, :TCML, :TCMax, :TCConf, :TCShape, :LMMin, :LMML, :LMMax, :LMConf, :LMShape)");
$statement->execute(array(
    "isCalculated" => false,
    "name" => $input->name,
    "iterations" => $input->iterations,
    "TEFMin" => $input->TEF->min,
    "TEFML" => $input->TEF->ml,
    "TEFMax" => $input->TEF->max,
    "TEFConf" => $input->TEF->confidence,
    "TEFShape" => $input->TEF->shape,
    "CSMin" => $input->CS->min,
    "CSML" => $input->CS->ml,
    "CSMax" => $input->CS->max,
    "CSConf" => $input->CS->confidence,
    "CSShape" => $input->CS->shape,
    "TCMin" => $input->TC->min,
    "TCML" => $input->TC->ml,
    "TCMax" => $input->TC->max,
    "TCConf" => $input->TC->confidence,
    "TCShape" => $input->TC->shape,
    "LMMin" => $input->LM->min,
    "LMML" => $input->LM->ml,
    "LMMax" => $input->LM->max,
    "LMConf" => $input->LM->confidence,
    "LMShape" => $input->LM->shape
));