<?php
/*
 * FILE         : DeleteScenario.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to delete a scenario from the database
 */

include 'Scenario.php';
include 'database_includes.php';

header('Content-type: application/json');

$input = file_get_contents('php://input');

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$pdo->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

$statement = $pdo->prepare("DELETE FROM scenarios WHERE id=:id");
if($statement->execute(array(
    "id" => $input
)))
{
    echo "1";
}