<?php
/*
 * FILE         : GetAllScenarios.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to get all active scenarios from the database
 */

include 'Scenario.php';
include 'database_includes.php';

header('Content-type: application/json');

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$statement=$pdo->prepare("SELECT * FROM scenarios");
$statement->execute();
$results=$statement->fetchAll(PDO::FETCH_ASSOC);

$scenarios = array();
foreach ($results as $line)
{
    $temp = new Scenario;
    $tempTEF = new ThreatEventFrequency;
    $tempLM = new LossMagnitude;
    $tempTC = new ThreatCapability;
    $tempCS = new ControlStrength;
    
    $temp->name = $line['name'];
    $temp->id = $line['id'];

    $tempTEF->min = $line['TEFMin'];
    $tempTEF->ml = $line['TEFML'];
    $tempTEF->max = $line['TEFMax'];
    $tempTEF->confidence = $line['TEFConf'];
    $temp->threat_event_frequency = $tempTEF;

    $tempLM->min = $line['LMMin'];
    $tempLM->ml = $line['LMML'];
    $tempLM->max = $line['LMMax'];
    $tempLM->confidence = $line['LMConf'];
    $temp->loss_magnitude = $tempLM;

    $tempTC->min = $line['TCMin'];
    $tempTC->ml = $line['TCML'];
    $tempTC->max = $line['TCMax'];
    $tempTC->confidence = $line['TCConf'];
    $temp->threat_capability= $tempTC;

    $tempCS->min = $line['CSMin'];
    $tempCS->ml = $line['CSML'];
    $tempCS->max = $line['CSMax'];
    $tempCS->confidence = $line['CSConf'];
    $temp->control_strength = $tempCS;
    
    array_push($scenarios,$temp);
}

$json=json_encode($scenarios);
echo $json;