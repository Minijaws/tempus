<?php
/*
 * FILE         : GetSingleScenario.php
 * PROJECT      : Tempus
 * PROGRAMMER   : Lorissa Becker
 * DATE         : March 2017
 * DESCTIPTION  : Used to get a single scenario from the database
 */

include 'Scenario.php';
include 'database_includes.php';

header('Content-type: application/json');

$scenarioID = $_GET['id'];

$pdo=new PDO("mysql:dbname=$dbname;host=$dbhost",$dbuser,$dbpw);
$statement=$pdo->prepare("SELECT * FROM scenarios WHERE id = $scenarioID");
$statement->execute();
$results=$statement->fetchAll(PDO::FETCH_ASSOC);
$line=$results[0];

$temp = new Scenario;
$tempTEF = new ThreatEventFrequency;
$tempLM = new LossMagnitude;
$tempTC = new ThreatCapability;
$tempCS = new ControlStrength;

$temp->name = $line['name'];
$temp->id = $line['id'];
$temp->iterations = $line['iterations'];

$tempTEF->min = $line['TEFMin'];
$tempTEF->ml = $line['TEFML'];
$tempTEF->max = $line['TEFMax'];
$tempTEF->confidence = $line['TEFConf'];
$tempTEF->shape = $line['TEFShape'];
$temp->threat_event_frequency = $tempTEF;

$tempLM->min = $line['LMMin'];
$tempLM->ml = $line['LMML'];
$tempLM->max = $line['LMMax'];
$tempLM->confidence = $line['LMConf'];
$tempLM->shape = $line['LMShape'];
$temp->loss_magnitude = $tempLM;

$tempTC->min = $line['TCMin'];
$tempTC->ml = $line['TCML'];
$tempTC->max = $line['TCMax'];
$tempTC->confidence = $line['TCConf'];
$tempTC->shape = $line['TCShape'];
$temp->threat_capability= $tempTC;

$tempCS->min = $line['CSMin'];
$tempCS->ml = $line['CSML'];
$tempCS->max = $line['CSMax'];
$tempCS->confidence = $line['CSConf'];
$tempCS->shape = $line['CSShape'];
$temp->control_strength = $tempCS;

$json=json_encode($temp);
echo $json;