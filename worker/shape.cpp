/*
* FILE			: shape.cpp
* PROJECT		: Tempus
* PROGRAMMER	: Joshua Gowing
* DATE			: February 2017
* DESCRIPTION	: Manages the inputs for a Monte Carlo simulation.
*/

#include "shape.h"


/*
* NAME			: parseType
* PARAMETERS	: string inString
* RETURNS		: Shape
* DESCRIPTION	: Turns the input from the front end into one of the Shape enums
*/
Shape::Type Shape::parseType(std::string const& inString)
{
	if (inString == "Binomial") return BINOMIAL;
	if (inString == "Uniform") return UNIFORM;
	if (inString == "Triangle") return TRIANGLE;
	if (inString == "Beta") return BETA;
	//fallback
	return BETA;
}

/*
* NAME			: Shape Constructor
* DESCRIPTION	: Sets up the values for the monte carlo simulation based on the data shape the user chose
*/
Shape::Shape(double min, double ML, double max, double conf, Type type): min_(min), 
ML_(ML), max_(max), conf_(conf), type_(type)
{
	switch (type_)
	{
	case BETA:
		if(min_>max_)
		{
//			min_ = nextafter(max_, -std::numeric_limits<double>::max());
		}
		a_ = 1 + conf_ * ((ML_ - min_) / (max_ - min_));
		b_ = 1 + conf_ * ((max_ - ML_) / (max_ - min_));
		break;
	case BINOMIAL:
		//act as Bernoulli trial with 2 options
		t_ = 1;
		p_ = (ML_ - min_) / (max_ - min_);
		break;
	case TRIANGLE:
		//Nothing needed
		break;
	case UNIFORM: 
		//Nothing needed
		break;
	default: ;
	}


	if(a_<0)
	{
		a_ = 1;
	}
	if(b_<0)
	{
		b_ = 1;
	}
}

/*
 * NAME	: Shape Destructor
 */
Shape::~Shape()
{
}

