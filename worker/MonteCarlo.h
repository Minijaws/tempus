#pragma once

#include "shape.h"
#include <random>
#include <boost/random.hpp>
class MonteCarlo
{
public:
	boost::mt19937 rng;
	boost::random::beta_distribution<double> distribution_;
	double *values_;
	int count_;
	Shape* shape_;
	double min_;
	double max_;
	void generate();
	MonteCarlo(Shape* shape, int count);
	~MonteCarlo();
private:
	std::random_device rd;
};


