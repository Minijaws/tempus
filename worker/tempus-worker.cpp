/*
* FILE			: tempus-worker.cpp
* PROJECT		: Tempus
* PROGRAMMER	: Joshua Gowing
* DATE			: February 2017
* DESCRIPTION	: This is the entirety of the worker. 
*/

#include <iostream>
#include "shape.h"
#include "MonteCarlo.h"
#include <zmq.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <thread>

/*
* NAME			: waitforwork
* PARAMETERS	: int threatID
				  context_t* context
* RETURNS		: void
* DESCRIPTION	: The main function that processes work being sent by the server.
*/
void waitforwork(int threadID,zmq::context_t* context)
{
	//  Socket to receive messages on
	zmq::socket_t receiver(*context, ZMQ_PULL);
	receiver.connect("inproc://workin");
	int linger = 0;
//	receiver.setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
	//  Socket to send messages to
	zmq::socket_t sender(*context, ZMQ_PUSH);
	sender.connect("inproc://workout");
	
	//Process tasks forever
	while (true)
	{
		zmq::message_t message;
		std::cout << "Thread:" << threadID << " waiting for message" << std::endl;
		receiver.recv(&message);
		std::cout << "Thread:" << threadID << " Got Message" << std::endl;
		boost::property_tree::ptree pt;
		std::string smessage(static_cast<char*>(message.data()), message.size());
		std::istringstream iss(smessage);
		read_json(iss, pt);

		boost::property_tree::ptree results;
		//  Do the work
		int count = pt.get<int>("iterations");
		double TEFmax = pt.get<double>("TEFMax");
		Shape TEFshape(pt.get<double>("TEFMin"), pt.get<double>("TEFML"), TEFmax, pt.get<double>("TEFConf"), Shape::parseType(pt.get<std::string>("TEFShape")));
		MonteCarlo TEF(&TEFshape, count);
		Shape TCshape(pt.get<double>("TCMin"), pt.get<double>("TCML"), pt.get<double>("TCMax"), pt.get<double>("TCConf"), Shape::parseType(pt.get<std::string>("TCShape")));
		MonteCarlo TC(&TCshape, count);
		Shape CSshape(pt.get<double>("CSMin"), pt.get<double>("CSML"), pt.get<double>("CSMax"), pt.get<double>("CSConf"), Shape::parseType(pt.get<std::string>("CSShape")));
		MonteCarlo CS(&CSshape, count);
		Shape LMshape(pt.get<double>("LMMin"), pt.get<double>("LMML"), pt.get<double>("LMMax"), pt.get<double>("LMConf"), Shape::parseType(pt.get<std::string>("LMShape")));
		MonteCarlo LM(&LMshape, count);

		bool* vulns = new bool[count];
		TEF.generate();
		TC.generate();
		CS.generate();
		LM.generate();
		results.put("id",pt.get<int>("id"));
		results.put("Count", count);
		boost::property_tree::ptree lossArray;
		for (int i = 0; i < count; i++)
		{
			double currcount = 0;
			double totalLoss = 0;
			if(TC.values_[i] > CS.values_[i])
			{
				currcount = TEF.values_[i];
				totalLoss = long(LM.values_[i]) * currcount;
				boost::property_tree::ptree loss;
				loss.put("Loss", totalLoss);
				loss.put("LossEvents", currcount);
				lossArray.push_back(std::make_pair("", loss));
			}
		}
		
		results.add_child("Losses", lossArray);
		std::stringstream json;
		boost::property_tree::write_json(json, results, false);
		std::string resultString = json.str();
		//std::cout << resultString << "\n";
		zmq::message_t reply(resultString.c_str(), resultString.length());
		//  Send results to sink
		sender.send(reply);
	}
	std::cout << "Thread:" << threadID << " done";
}

/*
* NAME			: proxy
* PARAMETERS	: socket_t* frontend
				  socket_t* backend
* RETURNS		: void
* DESCRIPTION	: Sets up the ZMQ proxy with the specified frontend and backend
*/
void proxy(zmq::socket_t* frontend, zmq::socket_t* backend){
	zmq::proxy(*frontend, *backend, NULL);
}

/*
* NAME			: main
* PARAMETERS	: none
* RETURNS		: int
*/
int main()
{
	unsigned concurentThreadsSupported = std::thread::hardware_concurrency();
	if (concurentThreadsSupported > 0)
	{
		concurentThreadsSupported--; //one less then number of cores
		
	}
	else
	{
		concurentThreadsSupported = 1;
	}

	// Reads from the config file for the sender address, receiver address and number of threads
	// unless a config file doesn't exist, in which case it sets the defaults and creates a one.
	boost::property_tree::ptree pt;
	pt.put("Receiver_address", "tcp://localhost:5557");
	pt.put("Sender_address", "tcp://localhost:5558");
	pt.put("Threads", concurentThreadsSupported);
	try
	{
		read_ini("config.ini", pt);
		concurentThreadsSupported = pt.get<int>("Threads");
		std::cout << "loaded config.ini" << std::endl;
	}
	catch (...)
	{
		write_ini("config.ini", pt);
		std::cout << "created default config.ini" << std::endl;
	}

	std::cout << "running on " << concurentThreadsSupported << " Threads" << std::endl;
	zmq::context_t context(1);
	zmq::socket_t receiver(context, ZMQ_PULL);
	receiver.connect(pt.get<std::string>("Receiver_address"));
	zmq::socket_t sender(context, ZMQ_PUSH);
	sender.connect(pt.get<std::string>("Sender_address"));

	zmq::socket_t workIn(context, ZMQ_PUSH);
	workIn.bind("inproc://workin");
	zmq::socket_t workOut(context, ZMQ_PULL);
	workOut.bind("inproc://workout");

	std::vector<std::thread> threads(concurentThreadsSupported+2);
	int threadID = 0;
	for (; threadID < concurentThreadsSupported; threadID++)
	{
		threads[threadID]=(std::thread(waitforwork, threadID,&context));
	}
	threads[threadID++] = (std::thread(proxy,&receiver,&workIn));
	threads[threadID++] = (std::thread(proxy,&workOut,&sender));
	std::cin.get();
	for (std::thread& thread : threads)
	{
		thread.join();
	}
}

// old Algorithm
// Not sure which is more accurate, the new algorithm or the old. Can be swapped out if needed.

//int count = pt.get<int>("iterations");
//double TEFmax = pt.get<double>("TEFMax");
//Shape TEFshape(pt.get<double>("TEFMin"), pt.get<double>("TEFML"), TEFmax, pt.get<double>("TEFConf"));
//MonteCarlo TEF(&TEFshape, count);
//Shape TCshape(pt.get<double>("TCMin"), pt.get<double>("TCML"), pt.get<double>("TCMax"), pt.get<double>("TCConf"));
//MonteCarlo TC(&TCshape, TEFmax);
//Shape CSshape(pt.get<double>("CSMin"), pt.get<double>("CSML"), pt.get<double>("CSMax"), pt.get<double>("CSConf"));
//MonteCarlo CS(&CSshape, TEFmax);
//Shape LMshape(pt.get<double>("LMMin"), pt.get<double>("LMML"), pt.get<double>("LMMax"), pt.get<double>("LMConf"));
//MonteCarlo LM(&LMshape, TEFmax);
//
//bool* vulns = new bool[count];
//TEF.generate();
//results.put("id", pt.get<int>("id"));
//results.put("Count", count);
//boost::property_tree::ptree lossArray;
//for (int i = 0; i < count; i++)
//{
//	int currcount = round(TEF.values_[i]);
//	TC.count_ = currcount;
//	CS.count_ = currcount;
//	TC.generate();
//	CS.generate();
//	int losses = 0;
//
//	for (int x = 0; x < currcount; x++)
//	{
//		losses += (vulns[i] = TC.values_[x] >
//			CS.values_[x]);
//	}
//	//std::cout << losses << "\n";
//	LM.count_ = losses;
//	LM.generate();
//	int totalLoss = 0;
//	for (int y = 0; y < losses; y++)
//	{
//		totalLoss += LM.values_[y];
//	}
//	boost::property_tree::ptree loss;
//	loss.put("Loss", totalLoss);
//	loss.put("LossEvents", losses);
//	lossArray.push_back(std::make_pair("", loss));
//}
