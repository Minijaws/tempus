/*
 * FILE			: MonteCarlo.cpp
 * PROJECT		: Tempus
 * PROGRAMMER	: Joshua Gowing
 * DATE			: February 2017
 * DESCRIPTION	: Contains all the logic for doing a Monte Carlo simulation.
 */

#include "MonteCarlo.h"
#include "math.h"
#include "stdio.h"
#include <boost/math/distributions.hpp>

/*
 * NAME			: generate
 * PARAMETERS	: none
 * RETURNS		: void
 * DESCRIPTION	: Seeds the random number generator and generates a set of values based on the selected data shape and min, max and most likely numbers
 */
void MonteCarlo::generate()
{
	rng.seed(rd());
	switch (shape_->type_)
	{
	case Shape::BETA:
		{s
			boost::random::beta_ditribution<double>
				gen_beta(shape_->a_, shape_->b_);
			//boost::variate_generator<boost::mt19937, boost::random::beta_distribution<double>> gen_beta(rng, beta_distribution);
			for (int i = 0; i < count_; i++)
			{
				values_[i] = (gen_beta(rng) * (max_ - min_) + min_);
			}
			break;
		}

	case Shape::BINOMIAL:
		{
			boost::random::bernoulli_distribution<bool>
				gen_binomial(shape_->p_);
			for (int i = 0; i < count_; i++)
			{
				values_[i] = gen_binomial(rng) ? shape_->max_ : shape_->min_;
			}
			break;
		}

	case Shape::TRIANGLE:
		{
			boost::random::triangle_distribution<double>
				gen_triangle(shape_->min_, shape_->ML_, shape_->max_);
			for (int i = 0; i < count_; i++)
			{
				values_[i] = gen_triangle(rng);
			}
			break;
		}

	case Shape::UNIFORM:
		{
			boost::random::uniform_real_distribution<double>
				gen_uniform(shape_->min_, shape_->max_);
			for (int i = 0; i < count_; i++)
			{
				values_[i] = gen_uniform(rng);
			}
			break;
		}
	}
}

/*
* NAME	: MonteCarlo Constructor
*/
MonteCarlo::MonteCarlo(Shape* shape, int count)
	: count_(count), shape_(shape), min_(shape_->min_), max_(shape_->max_)
{
	values_ = new double[count];
	rng.seed(rd());
}

/*
 * NAME	: MonteCarlo Destructor
 */
MonteCarlo::~MonteCarlo()
{
	delete values_;
}
