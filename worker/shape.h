#pragma once
#include <string>
#include <math.h>
class Shape
{
public:
	//UNIFORM
	//TRIANGLE
	double min_;
	double ML_;
	double max_;
	double conf_;

	//BETA
	double a_;
	double b_;
	
	//BINOMIAL
	int t_;
	double p_;



	enum Type
	{
		BETA,
		BINOMIAL,
		TRIANGLE,
		UNIFORM
	};

	static Type parseType(std::string const& inString);
	Type type_;
	Shape(double min, double ML, double max, double conf,Type type);
	~Shape();
};


